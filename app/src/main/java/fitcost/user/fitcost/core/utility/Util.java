package fitcost.user.fitcost.core.utility;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.format.DateFormat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import fitcost.user.fitcost.R;

public class Util {

    public static String getTwoDigitNum(int x) {
        if (x < 10) return "0" + x;
        else return String.valueOf(x);
    }

    public static String getCustomizeDate(long timestamp) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(timestamp);
        return DateFormat.format("EEEE, dd MMM yyyy hh:mm", cal).toString();
    }

    public static void createAlertDialog(Context context,
                                         String messsage,
                                         String title,
                                         DialogInterface.OnClickListener positiveCallback) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title)
                .setMessage(messsage)
                .setPositiveButton(android.R.string.yes, positiveCallback)
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .show();
    }

    public static String getStringFromEditText(EditText edt) {
        return edt.getText().toString();
    }

    public static boolean isNumeric(String s) {
        try {
            Long.parseLong(s);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
