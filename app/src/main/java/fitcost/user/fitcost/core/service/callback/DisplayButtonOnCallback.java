package fitcost.user.fitcost.core.service.callback;

public interface DisplayButtonOnCallback {
    void show();
    void hide();
}
