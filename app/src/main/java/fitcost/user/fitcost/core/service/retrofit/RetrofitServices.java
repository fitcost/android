package fitcost.user.fitcost.core.service.retrofit;


import fitcost.user.fitcost.core.service.ApiServices;

public class RetrofitServices {
    public static ApiServices.AccountService requestAccount() {
        return RetrofitClient.client().create(ApiServices.AccountService.class);
    }
}
