package fitcost.user.fitcost.core.base;

public interface BaseLifecycle {
    void onPause();
    void onResume();
    void onDestroy();
}
