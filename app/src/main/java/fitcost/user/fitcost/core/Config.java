package fitcost.user.fitcost.core;

import android.Manifest;

import java.util.HashMap;

public class Config {
    public static final String dbname = "mydatabase";
    public static final int version = 1;
    public static final String BASE_URL = "https://www.google.com";
    public static final int minPasswordLength = 6;
    public static final long SPLASH_TIMEOUT = 2500;
    public static HashMap<String, Integer> permissions = getPermissions();

    private static final int internetCode = 1001;

    private static HashMap<String, Integer> getPermissions() {
        HashMap<String, Integer> permissions = new HashMap<>();
        permissions.put(Manifest.permission.INTERNET, internetCode);
        return permissions;
    }
}
