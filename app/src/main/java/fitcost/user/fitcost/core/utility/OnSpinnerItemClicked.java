package fitcost.user.fitcost.core.utility;

public interface OnSpinnerItemClicked {
    void onClick(int position, String item);
}
