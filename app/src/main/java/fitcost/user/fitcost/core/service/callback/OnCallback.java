package fitcost.user.fitcost.core.service.callback;

public interface OnCallback<T> {
    void onSuccess(T data);
    void onFailed();
}
