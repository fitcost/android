package fitcost.user.fitcost.feature.forgot;

import fitcost.user.fitcost.core.base.BaseActivityView;
import fitcost.user.fitcost.core.base.BaseLifecycle;

public interface ForgotPasswordListener {

    interface View extends BaseActivityView {
        void navigateToLogin();
    }

    interface Presenter extends BaseLifecycle {

    }

}
