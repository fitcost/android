package fitcost.user.fitcost.feature.home.inputglaccount;

import java.util.List;

import fitcost.user.fitcost.core.base.BaseFragmentView;
import fitcost.user.fitcost.core.base.BaseLifecycle;
import fitcost.user.fitcost.feature.home.HomeListener;

public interface InputGLAccountListener {
    interface View extends BaseFragmentView<HomeListener.Presenter> {
        void initializeAccountSpinner(List<String> list);
        void close();
    }

    interface Presenter extends BaseLifecycle {
        void loadSpinnerAccountData();
    }
}
