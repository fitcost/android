package fitcost.user.fitcost.feature.profile;

import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import fitcost.user.fitcost.R;
import fitcost.user.fitcost.core.base.BaseActivity;
import fitcost.user.fitcost.core.base.BaseLifecycle;
import fitcost.user.fitcost.feature.Login.LoginActivity;
import fitcost.user.fitcost.feature.balancedetail.BalanceDetailActivity;
import fitcost.user.fitcost.feature.home.HomeActivity;

public class ProfileActivity extends BaseActivity implements ProfileListener.View, BaseLifecycle {

    ProfileListener.Presenter presenter = new ProfilePresenter(this);

    @BindView(R.id.spinner_month)
    Spinner spinnerMonth;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.btn_detail)
    CardView btnDetail;
    @BindView(R.id.sign_out)
    RelativeLayout signOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        render();
        presenter.loadSpinnerAndDataMonth();
    }

    @Override
    public void render() {
        super.render();
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigateToHome();
            }
        });
        btnDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigateToDetailTransaction();
            }
        });
        signOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigateToLogin();
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void initializeMonthSpinner(List<String> list) {
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getApplicationContext(), R.layout.spinner_item_text, list);

        spinnerMonth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String selectedItem = (String) adapterView.getItemAtPosition(i);
                Toast.makeText(ProfileActivity.this, selectedItem, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerMonth.setAdapter(arrayAdapter);
    }

    @Override
    public void navigateToLogin() {
        goToActivity(LoginActivity.class, true, true);
    }

    @Override
    public void navigateToDetailTransaction() {
        goToActivity(BalanceDetailActivity.class);
    }

    @Override
    public void navigateToHome() {
        finish();
    }
}
