package fitcost.user.fitcost.feature.home;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import fitcost.user.fitcost.R;
import fitcost.user.fitcost.core.adapter.RecyclerViewAdapter;
import fitcost.user.fitcost.core.base.BaseView;
import fitcost.user.fitcost.feature.home.model.RecurringTransaction;

public class RecurringAdapter extends RecyclerViewAdapter<RecurringTransaction, RecurringAdapter.ViewHolder> {

    public RecurringAdapter(List<RecurringTransaction> list, BaseView baseView) {
        super(list, baseView);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_recurring_transaction,viewGroup,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final RecurringTransaction recurringTransaction = list.get(i);

        viewHolder.txtName.setText(recurringTransaction.getName());
        viewHolder.txtVal.setText(recurringTransaction.getValue());
        viewHolder.txtVal.setTextColor(baseView.getApplicationContext().getResources().getColor(recurringTransaction.getCategoryColor()));
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView txtName;
        TextView txtVal;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtName = itemView.findViewById(R.id.txt_transaction);
            txtVal = itemView.findViewById(R.id.txt_val_transaction);
        }
    }


}
