package fitcost.user.fitcost.feature.Landing;

import fitcost.user.fitcost.core.base.BaseActivityView;
import fitcost.user.fitcost.core.base.BaseLifecycle;

public interface LandingListener {

    interface View extends BaseActivityView {
        void navigateToLogin();
    }

    interface Presenter extends BaseLifecycle {
        void initializeLocaldb();
        void goToAllowedDestination();
    }
}
