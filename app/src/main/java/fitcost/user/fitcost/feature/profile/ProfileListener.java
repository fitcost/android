package fitcost.user.fitcost.feature.profile;

import java.util.List;

import fitcost.user.fitcost.core.base.BaseActivityView;
import fitcost.user.fitcost.core.base.BaseLifecycle;

public interface ProfileListener {
    interface View extends BaseActivityView {
        void initializeMonthSpinner(List<String> list);
        void navigateToLogin();
        void navigateToDetailTransaction();
        void navigateToHome();
    }

    interface Presenter extends BaseLifecycle {
        void loadSpinnerAndDataMonth();
    }
}
