package fitcost.user.fitcost.feature.home;

import java.util.ArrayList;
import java.util.List;

import fitcost.user.fitcost.R;
import fitcost.user.fitcost.feature.home.model.RecurringTransaction;

public class HomePresenter implements HomeListener.Presenter {

    HomeListener.View view;

    private List<String> listMonth = new ArrayList<>();
    private List<RecurringTransaction> listRecurringTransaction = new ArrayList<>();
    private RecurringAdapter adapter;

    public HomePresenter(HomeListener.View view) {
        this.view = view;
    }

    @Override
    public void onPause() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onDestroy() {
        view = null;
    }

    @Override
    public void loadSpinnerAndDataMonth() {
        //call api

        listMonth.add("JANUARY 2019");
        listMonth.add("FEBRUARY 2019");
        listMonth.add("MARCH 2019");
        listMonth.add("APRIL 2019");
        view.initializeMonthSpinner(listMonth);
    }

    @Override
    public void loadRecurringTransactions() {
        listRecurringTransaction.add(new RecurringTransaction("Transaction 1", "Merchant", R.color.yellow_circle));
        listRecurringTransaction.add(new RecurringTransaction("Transaction 2", "Logistic", R.color.yellow_circle));
        listRecurringTransaction.add(new RecurringTransaction("Transaction 3", "Rejected", R.color.red_circle));

        adapter = new RecurringAdapter(listRecurringTransaction, view);
        view.setUpRecurringList(adapter);
    }

    @Override
    public void onGLAccountInputed() {

    }
}
