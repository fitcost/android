package fitcost.user.fitcost.feature.home;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import fitcost.user.fitcost.R;
import fitcost.user.fitcost.core.base.BaseActivity;
import fitcost.user.fitcost.core.base.BaseFragment;
import fitcost.user.fitcost.core.base.BaseLifecycle;
import fitcost.user.fitcost.feature.balancedetail.BalanceDetailActivity;
import fitcost.user.fitcost.feature.home.inputglaccount.InputGLAccountFragment;
import fitcost.user.fitcost.feature.profile.ProfileActivity;

public class HomeActivity extends BaseActivity implements HomeListener.View, BaseLifecycle {

    HomeListener.Presenter presenter = new HomePresenter(this);

    @BindView(R.id.spinner_month)
    Spinner spinnerMonth;
    @BindView(R.id.rv_recurring)
    RecyclerView rvRecurring;
    @BindView(R.id.img_account)
    ImageView imgAccount;
    @BindView(R.id.btn_balance_detail)
    CardView btnBalanceDetail;
    @BindView(R.id.order_section)
    CardView orderSection;
    @BindView(R.id.meeting_section)
    CardView meetingSection;
    @BindView(R.id.frame_layout)
    FrameLayout frameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        render();
        presenter.loadSpinnerAndDataMonth();
        presenter.loadRecurringTransactions();
    }

    @Override
    public void render() {
        super.render();
        imgAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigateToProfile();
            }
        });
        btnBalanceDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigateToBalanceDetail();
            }
        });

        View.OnClickListener inputGLAccountOnClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openInputGLAccount();
            }
        };
        orderSection.setOnClickListener(inputGLAccountOnClick);
        meetingSection.setOnClickListener(inputGLAccountOnClick);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void initializeMonthSpinner(List<String> list) {
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getApplicationContext(), R.layout.spinner_item_text, list);

        spinnerMonth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String selectedItem = (String) adapterView.getItemAtPosition(i);
                Toast.makeText(HomeActivity.this, selectedItem, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerMonth.setAdapter(arrayAdapter);
    }

    @Override
    public void setUpRecurringList(RecurringAdapter recurringList) {
        rvRecurring.setHasFixedSize(true);
        final LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext()){
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        rvRecurring.setLayoutManager(llm);
        rvRecurring.setAdapter(recurringList);
    }

    @Override
    public void openInputGLAccount() {
        BaseFragment<HomeListener.Presenter> fragment = new InputGLAccountFragment();
        fragment.attachPresenter(presenter);
        getSupportFragmentManager().beginTransaction()
                .add(R.id.frame_layout, fragment)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void navigateToProfile() {
        goToActivity(ProfileActivity.class);
    }

    @Override
    public void navigateToBalanceDetail() {
        goToActivity(BalanceDetailActivity.class);
    }
}
