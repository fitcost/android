package fitcost.user.fitcost.feature.forgot;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import fitcost.user.fitcost.R;
import fitcost.user.fitcost.core.base.BaseActivity;
import fitcost.user.fitcost.core.base.BaseLifecycle;

public class ForgotPasswordActivity extends BaseActivity implements ForgotPasswordListener.View, BaseLifecycle {

    ForgotPasswordListener.Presenter presenter = new ForgotPasswordPresenter(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        getWindow().setBackgroundDrawableResource(R.drawable.bg);
        render();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void navigateToLogin() {
        finish();
    }
}
