package fitcost.user.fitcost.feature.profile;

import java.util.ArrayList;
import java.util.List;

public class ProfilePresenter implements ProfileListener.Presenter {

    ProfileListener.View view;

    private List<String> listMonth = new ArrayList<>();

    public ProfilePresenter(ProfileListener.View view) {
        this.view = view;
    }

    @Override
    public void onPause() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onDestroy() {
        view = null;
    }

    @Override
    public void loadSpinnerAndDataMonth() {
        listMonth.add("JANUARY 2019");
        listMonth.add("FEBRUARY 2019");
        listMonth.add("MARCH 2019");
        listMonth.add("APRIL 2019");
        view.initializeMonthSpinner(listMonth);
    }
}
