package fitcost.user.fitcost.feature.home.model;

public class RecurringTransaction {
    private String name;
    private String value;
    private int categoryColor;

    public RecurringTransaction(String name, String value, int categoryColor) {
        this.name = name;
        this.value = value;
        this.categoryColor = categoryColor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getCategoryColor() {
        return categoryColor;
    }

    public void setCategoryColor(int categoryColor) {
        this.categoryColor = categoryColor;
    }
}
