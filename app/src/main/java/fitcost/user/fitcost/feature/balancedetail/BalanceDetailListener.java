package fitcost.user.fitcost.feature.balancedetail;

import java.util.List;

import fitcost.user.fitcost.core.base.BaseActivityView;
import fitcost.user.fitcost.core.base.BaseLifecycle;
import fitcost.user.fitcost.feature.balancedetail.model.BalanceDetail;

public interface BalanceDetailListener {
    interface View extends BaseActivityView {
        void setUpBalanceList(BalanceDetailAdapter adapter);
        void navigateToProfile();
    }

    interface Presenter extends BaseLifecycle {
        void loadBalanceDetail();
    }
}
