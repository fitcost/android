package fitcost.user.fitcost.feature.Login;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import fitcost.user.fitcost.R;
import fitcost.user.fitcost.core.base.BaseActivity;
import fitcost.user.fitcost.core.base.BaseLifecycle;
import fitcost.user.fitcost.feature.forgot.ForgotPasswordActivity;
import fitcost.user.fitcost.feature.home.HomeActivity;

public class LoginActivity extends BaseActivity implements LoginListener.View, BaseLifecycle {

    @BindView(R.id.btn_login)
    Button btnLogin;
    @BindView(R.id.txt_forgot_password)
    TextView txtForgotPassword;

    LoginListener.Presenter presenter = new LoginPresenter(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getWindow().setBackgroundDrawableResource(R.drawable.bg);
        render();
    }

    @Override
    public void render() {
        super.render();
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigateToHome();
            }
        });
        txtForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigateToForgotPassword();
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void navigateToHome() {
        goToActivity(HomeActivity.class, true, true);
    }

    @Override
    public void navigateToForgotPassword() {
        goToActivity(ForgotPasswordActivity.class);
    }
}
