package fitcost.user.fitcost.feature.balancedetail;

import java.util.ArrayList;
import java.util.List;

import fitcost.user.fitcost.feature.balancedetail.model.BalanceDetail;

public class BalanceDetailPresenter implements BalanceDetailListener.Presenter {

    BalanceDetailListener.View view;

    List<BalanceDetail> list = new ArrayList<>();
    BalanceDetailAdapter adapter;

    public BalanceDetailPresenter(BalanceDetailListener.View view) {
        this.view = view;
    }

    @Override
    public void onPause() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onDestroy() {
        view = null;
    }

    @Override
    public void loadBalanceDetail() {
        list.add(new BalanceDetail("AX10", "Rp10.000.000,00"));
        list.add(new BalanceDetail("AX10", "Rp10.000.000,00"));
        list.add(new BalanceDetail("AX10", "Rp10.000.000,00"));
        list.add(new BalanceDetail("AX10", "Rp10.000.000,00"));
        list.add(new BalanceDetail("AX10", "Rp10.000.000,00"));
        list.add(new BalanceDetail("AX10", "Rp10.000.000,00"));

        adapter = new BalanceDetailAdapter(list, view);
        view.setUpBalanceList(adapter);

    }
}
