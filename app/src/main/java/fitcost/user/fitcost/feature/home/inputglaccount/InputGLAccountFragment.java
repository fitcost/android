package fitcost.user.fitcost.feature.home.inputglaccount;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import fitcost.user.fitcost.R;
import fitcost.user.fitcost.core.base.BaseFragment;
import fitcost.user.fitcost.core.base.BaseLifecycle;
import fitcost.user.fitcost.feature.home.HomeListener;

/**
 * A simple {@link Fragment} subclass.
 */
public class InputGLAccountFragment extends BaseFragment<HomeListener.Presenter> implements InputGLAccountListener.View, BaseLifecycle {

    @BindView(R.id.btn_close)
    RelativeLayout btnClose;
    @BindView(R.id.btn_ok)
    Button btnOk;
    @BindView(R.id.btn_cancel)
    Button btnCancel;
    @BindView(R.id.spinner_gl_account)
    Spinner spinnerGlAccount;
    @BindView(R.id.frame)
    RelativeLayout frame;

    InputGLAccountListener.Presenter presenter = new InputGLAccountPresenter(this);

    public InputGLAccountFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_input_glaccount, container, false);
        render(v);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        presenter.loadSpinnerAccountData();
    }

    @Override
    public void render(View view) {
        super.render(view);
        frame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Just for prevent user click home acitvity
            }
        });
        View.OnClickListener closeListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                close();
            }
        };
        btnCancel.setOnClickListener(closeListener);
        btnClose.setOnClickListener(closeListener);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                basePresenter.onGLAccountInputed();
            }
        });
    }

    @Override
    public void initializeAccountSpinner(List<String> list) {
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getApplicationContext(), R.layout.spinner_item_text_black, list);

        spinnerGlAccount.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String selectedItem = (String) adapterView.getItemAtPosition(i);
                Toast.makeText(getApplicationContext(), selectedItem, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerGlAccount.setAdapter(arrayAdapter);
    }

    @Override
    public void close() {
        if (getFragmentManager() != null) {
            getFragmentManager().popBackStack();
        }
    }
}
