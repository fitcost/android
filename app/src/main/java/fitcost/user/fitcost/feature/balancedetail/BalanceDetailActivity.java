package fitcost.user.fitcost.feature.balancedetail;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import fitcost.user.fitcost.R;
import fitcost.user.fitcost.core.base.BaseActivity;
import fitcost.user.fitcost.core.base.BaseLifecycle;
import fitcost.user.fitcost.feature.balancedetail.model.BalanceDetail;

public class BalanceDetailActivity extends BaseActivity implements BalanceDetailListener.View, BaseLifecycle {

    @BindView(R.id.rv_balance)
    RecyclerView rvBalance;
    @BindView(R.id.img_back)
    ImageView imgBack;

    BalanceDetailListener.Presenter presenter = new BalanceDetailPresenter(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_balance_detail);
        render();
        presenter.loadBalanceDetail();
    }

    @Override
    public void render() {
        super.render();
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigateToProfile();
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void setUpBalanceList(BalanceDetailAdapter adapter) {
        rvBalance.setHasFixedSize(true);
        final LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext()){
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        rvBalance.setLayoutManager(llm);
        rvBalance.setAdapter(adapter);
    }

    @Override
    public void navigateToProfile() {
        finish();
    }
}
