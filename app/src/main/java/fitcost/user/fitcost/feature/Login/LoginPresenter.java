package fitcost.user.fitcost.feature.Login;

public class LoginPresenter implements LoginListener.Presenter {

    LoginListener.View view;

    public LoginPresenter(LoginListener.View view) {
        this.view = view;
    }

    @Override
    public void onPause() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onDestroy() {
        view = null;
    }
}
