package fitcost.user.fitcost.feature.Landing;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import butterknife.BindView;
import fitcost.user.fitcost.R;
import fitcost.user.fitcost.core.base.BaseActivity;
import fitcost.user.fitcost.core.base.BaseLifecycle;
import fitcost.user.fitcost.feature.Login.LoginActivity;

public class LandingActivity extends BaseActivity implements LandingListener.View, BaseLifecycle {

    LandingListener.Presenter presenter = new LandingPresenter(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);
        render();

        presenter.initializeLocaldb();
        presenter.goToAllowedDestination();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void navigateToLogin() {
        goToActivity(LoginActivity.class, true);
    }
}
