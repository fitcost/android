package fitcost.user.fitcost.feature.forgot;

public class ForgotPasswordPresenter implements ForgotPasswordListener.Presenter {

    ForgotPasswordListener.View view;

    public ForgotPasswordPresenter(ForgotPasswordListener.View view) {
        this.view = view;
    }

    @Override
    public void onPause() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onDestroy() {
        view = null;
    }
}
