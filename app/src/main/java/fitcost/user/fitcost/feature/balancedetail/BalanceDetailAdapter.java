package fitcost.user.fitcost.feature.balancedetail;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import fitcost.user.fitcost.R;
import fitcost.user.fitcost.core.adapter.RecyclerViewAdapter;
import fitcost.user.fitcost.core.base.BaseView;
import fitcost.user.fitcost.feature.balancedetail.model.BalanceDetail;

public class BalanceDetailAdapter extends RecyclerViewAdapter<BalanceDetail, BalanceDetailAdapter.ViewHolder> {

    public BalanceDetailAdapter(List<BalanceDetail> list, BaseView baseView) {
        super(list, baseView);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_balance_detail,viewGroup,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final BalanceDetail balanceDetail = list.get(i);

        viewHolder.txtId.setText(balanceDetail.getId());
        viewHolder.txtValue.setText(balanceDetail.getValue());
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtId;
        TextView txtValue;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtId = itemView.findViewById(R.id.txt_id_balance);
            txtValue = itemView.findViewById(R.id.txt_balance);
        }
    }
}
