package fitcost.user.fitcost.feature.home;

import java.util.List;

import fitcost.user.fitcost.core.base.BaseActivityView;
import fitcost.user.fitcost.core.base.BaseLifecycle;

public interface HomeListener {
    interface View extends BaseActivityView {
        void initializeMonthSpinner(List<String> list);
        void setUpRecurringList(RecurringAdapter recurringList);
        void openInputGLAccount();
        void navigateToProfile();
        void navigateToBalanceDetail();
    }

    interface Presenter extends BaseLifecycle {
        void loadSpinnerAndDataMonth();
        void loadRecurringTransactions();
        void onGLAccountInputed();
    }
}
