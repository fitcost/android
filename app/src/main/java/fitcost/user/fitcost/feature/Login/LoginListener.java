package fitcost.user.fitcost.feature.Login;

import fitcost.user.fitcost.core.base.BaseActivityView;
import fitcost.user.fitcost.core.base.BaseLifecycle;

public interface LoginListener {

    interface View extends BaseActivityView {
        void navigateToHome();
        void navigateToForgotPassword();
    }

    interface Presenter extends BaseLifecycle {

    }

}
