package fitcost.user.fitcost.feature.home.inputglaccount;

import java.util.ArrayList;
import java.util.List;

public class InputGLAccountPresenter implements InputGLAccountListener.Presenter {

    InputGLAccountListener.View view;

    List<String> listAccount = new ArrayList<>();

    public InputGLAccountPresenter(InputGLAccountListener.View view) {
        this.view = view;
    }

    @Override
    public void onPause() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onDestroy() {
        view = null;
    }

    @Override
    public void loadSpinnerAccountData() {
        //call api

        listAccount.add("FSS");
        listAccount.add("GLX");
        listAccount.add("ASV");
        listAccount.add("HRW");
        view.initializeAccountSpinner(listAccount);
    }
}
