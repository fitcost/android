package fitcost.user.fitcost.feature.Landing;

import android.os.Handler;
import android.widget.Toast;

import com.raizlabs.android.dbflow.config.FlowConfig;
import com.raizlabs.android.dbflow.config.FlowManager;

import fitcost.user.fitcost.core.Config;


public class LandingPresenter implements LandingListener.Presenter {

    LandingListener.View view;

    private Handler handler;
    private Runnable runnable;


    public LandingPresenter(LandingListener.View view) {
        this.view = view;
        handler = new Handler();
    }

    @Override
    public void onPause() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onDestroy() {
        view = null;
        if (runnable != null) {
            handler.removeCallbacks(runnable);
        }
    }

    @Override
    public void initializeLocaldb() {
        FlowManager.init(new FlowConfig.Builder(view.getApplicationContext()).build());
    }

    @Override
    public void goToAllowedDestination() {
        runnable = new Runnable() {
            @Override
            public void run() {
                boolean allowed = view.checkPermissions();
                if (allowed) {
//            if (account == null) view.navigateToHome();
                    view.navigateToLogin();
                }
            }
        };
        handler.postDelayed(runnable, Config.SPLASH_TIMEOUT);
    }
}
